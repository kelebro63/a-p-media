package pandarium.android.calendar.database;


import pandarium.android.R;
import pandarium.android.calendar.model.CDI;
import pandarium.android.calendar.model.CDI.EventType;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;


public class DataActivity extends ActionBarActivity implements OnClickListener, ActionBar.OnNavigationListener {  

	public static DB db_Notes;
    public static DB db_Alarms;
    public static DB db_Events;
	static ViewPager pager;
	static PagerAdapter pagerAdapter;
	static int PAGE_COUNT = 3;
	private ActionBar actionBar;
	private static DataActivity activity;

	@Override
	protected void onCreate(Bundle savedInstanceState)  {

		super.onCreate(savedInstanceState);	
		setContentView(R.layout.tsk);
		actionBar = getSupportActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#D3F3D3")));
		setCurrentTitle(0);
		createInstancesDB();
		activity = this;
		pager = (ViewPager) findViewById(R.id.pager);
		pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
		pager.setOffscreenPageLimit(0);
		pager.setAdapter(pagerAdapter);
		pager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				setCurrentTitle(position);
				try {
					PageFragment.closeActionMode();
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			@Override
			public void onPageScrolled(int position, float positionOffset,
					int positionOffsetPixels) {
			}

			@Override
			public void onPageScrollStateChanged(int state) {
			}
		});

	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		getMenuInflater().inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);

	}




	@Override
	protected void onDestroy() {
		super.onDestroy();
	}	

	@Override
	public void onClick(View v) {
	}	

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {

		return super.onContextItemSelected(item);
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onStop() {
		super.onStop();
	}

	@Override
	protected void onRestart() {
		super.onRestart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		try {
			DataActivity.update();
		} catch (Exception e) {
		}
	}

	public boolean onOptionsItemSelected(
			MenuItem item) {
		switch (item.getItemId()) {
		case R.id.itemadd:
			
			switch (pager.getCurrentItem()) {
			case 0:
				Intent intentAddAlarm = new Intent(this, AlarmActivity.class);
				intentAddAlarm.putExtra("flag", 0);
				startActivity(intentAddAlarm);
				break;
			case 1:
				Intent intentAddNote = new Intent(this, NoteActivity.class);
				intentAddNote.putExtra("flag", 0);
				startActivity(intentAddNote);
				break;
			case 2:
				Intent intentAddEvent = new Intent(this, EventActivity.class);
				intentAddEvent.putExtra("flag", 0);
				startActivity(intentAddEvent);	
				break;	
			default:
				break;
			}
			return true;
		}
		return false;
	}

	@Override
	public boolean onNavigationItemSelected(int arg0, long arg1) {

		return false;
	}
	
	public static DataActivity getActivity() {
		return activity;
	}
	
	public static void update() {
		// pager.setAdapter(pagerAdapter);
		//	refreshView(pager.getCurrentItem());
		pagerAdapter.notifyDataSetChanged();


	}
	
	public void setCurrentTitle(int pagePosition) {
		switch (pagePosition) {
		case 0:
			actionBar.setTitle("ALARMS");
			break;
		case 1:
			actionBar.setTitle("NOTES");
			break;
		case 2:
			actionBar.setTitle("EVENTS");
			break;
		default:
			break;
		}
	}

	private void createInstancesDB() {
		db_Notes = new DB(this);
		db_Notes.open(DB.DB_NAME_NOTES);

		db_Alarms = new DB(this);
		db_Alarms.open(DB.DB_NAME_ALARMS);

		db_Events = new DB(this);
		db_Events.open(DB.DB_NAME_EVENTS);
	}

	private class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {

		public MyFragmentPagerAdapter(FragmentManager fm) {
			super(fm);
		}



		@Override
		public Fragment getItem(int position) {
			return PageFragment.newInstance(position);
		}

		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}


		public void setCount(int i) {
			PAGE_COUNT = i;
		}

		@Override
		public int getCount() {
			return PAGE_COUNT;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return "Title " + position;
		}

		// ��� �� FragmentPagerAdapter.java

		private String makeFragmentName(int viewId, long id) {
			return "android:switcher:" + viewId + ":" + id;
		}

	}

}

