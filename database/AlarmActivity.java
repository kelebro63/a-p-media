package pandarium.android.calendar.database;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import pandarium.android.R;
import pandarium.android.calendar.model.CDI;
import pandarium.android.calendar.model.events.Alarm;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

public class AlarmActivity extends ActionBarActivity implements OnClickListener{

	public static final int TIME_DIALOG_ID = 999;
	public static final int DATE_DIALOG_ID = 998;

	static Button timeButton;
	static Button dateButton;
	EditText eName, eDesc;
	TextView repeatType;
	CheckBox isRepeat;
	static int year, month, day, hour, minute;
	ActionBar actionBar;
	Integer flagEditEvent = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState)  {

		super.onCreate(savedInstanceState);	
		setContentView(R.layout.alarm);
		actionBar = getSupportActionBar();
		actionBar.setTitle("AlarmsActivity");
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#D3F3D3")));
		timeButton = (Button) findViewById(R.id.time_up);
		timeButton.setOnClickListener(this);
		dateButton = (Button) findViewById(R.id.dateUp);
		dateButton.setOnClickListener(this);
		eName = (EditText) findViewById(R.id.eName);
		eDesc = (EditText) findViewById(R.id.eDesc);
		isRepeat = (CheckBox) findViewById(R.id.checkIsRepeat);
		isRepeat.setOnClickListener(this);
		repeatType = (TextView) findViewById(R.id.repeatType);
		Intent intent = getIntent();
		flagEditEvent = intent.getIntExtra("flag", 0);
		if (flagEditEvent == 1) {	
			Alarm event = (Alarm) getIntent().getParcelableExtra("test");
			Toast.makeText(this, "проверяем",
					Toast.LENGTH_LONG).show();
			eName.setText(event.getHeader());
			eDesc.setText(event.getDescription());
			timeButton.setText(new StringBuilder().append(pad(event.getCdi().getEventHH())).append(":").append(new StringBuilder().append(pad(event.getCdi().getEventMM()))));
			dateButton.setText(new StringBuilder().append(pad(event.getCdi().getEventDay())).append("-").append(new StringBuilder().append(pad(event.getCdi().getEventMonth()))).append("-").append(new StringBuilder().append(pad(event.getCdi().getEventYear()))));
			if (event.isRepeated()) {
				isRepeat.setChecked(true);
			} else {
				isRepeat.setChecked(false);
			}
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.addeventmenu, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.save:

			if ((dateButton.getText().length() == 0) || (timeButton.getText().length() == 0) || (eName.getText().length() == 0) ) {
				Toast.makeText(this, "Not all fields are filled", 100).show();
			} else {
				
				if (flagEditEvent == 0) {
					Alarm event = new Alarm(eName.getText().toString(), eDesc.getText().toString());
					event.setCdi(new CDI.Builder().setType(CDI.EventType.ALARM).setYear(year).setMonth(month).setDay(day).setHours(hour).setMinutes(minute).build());
					if (isRepeat.isChecked()) {
						event.setRepeated(true);
					} else {
						event.setRepeated(false);
					}
					event.setmRepeatType(repeatType.getText().toString());//заглушка
					event.setId(DataActivity.db_Alarms.addEvent(event));
				} else {
					Alarm event = (Alarm) getIntent().getParcelableExtra("test");
					event.setHeader(eName.getText().toString());
					event.setDescription(eDesc.getText().toString());
					event.setCdi(new CDI.Builder().setType(CDI.EventType.ALARM).setYear(year).setMonth(month).setDay(day).setHours(hour).setMinutes(minute).build());
					if (isRepeat.isChecked()) {
						event.setRepeated(true);
					} else {
						event.setRepeated(false);
					}
					event.setmRepeatType(repeatType.getText().toString());//заглушка
					DataActivity.db_Alarms.updateEvent(event);
				}
				finish();
			}
			break;
		case android.R.id.home:

			Intent intentHome = NavUtils.getParentActivityIntent(this); 
			intentHome.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP); 
			NavUtils.navigateUpTo(this, intentHome);
			return true;	
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.time_up:
			showDialog(TIME_DIALOG_ID);
			break;
		case R.id.dateUp:
			showDialog(DATE_DIALOG_ID);
			break;	
		case R.id.checkIsRepeat:
			break;	
		}
	}

	@Override
	protected Dialog onCreateDialog(int id) {
		switch (id) {
		case TIME_DIALOG_ID:
			// set time picker as current time
			String time =  (String) timeButton.getText();
			//int hour, minute;
			try {
				hour = Integer.parseInt((String) time.substring(0, 2));
				minute = Integer.parseInt((String) time.substring(3));

			} catch (Exception e) {
				hour = 6;
				minute = 0;
			}
			return new TimePickerDialog(this, 
					timePickerListener, hour, minute,true);

		case DATE_DIALOG_ID:
			//int year, month,day;
			try {
				Calendar cal = Calendar.getInstance();
				SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");//("yyyy-MM-dd");
				cal.setTime(format.parse(dateButton.getText().toString()));
				year = cal.get(Calendar.YEAR);
				month = cal.get(Calendar.MONTH);
				day = cal.get(Calendar.DAY_OF_MONTH);
			} catch (ParseException e) {
				Calendar cal = Calendar.getInstance();
				year = cal.get(Calendar.YEAR);
				month = cal.get(Calendar.MONTH);
				day = cal.get(Calendar.DAY_OF_MONTH);
			}
			return new DatePickerDialog(this, datePickerListener, 
					year, month, day);	
		}

		return null;
	}

	private TimePickerDialog.OnTimeSetListener timePickerListener = 
			new TimePickerDialog.OnTimeSetListener() {


		public void onTimeSet(TimePicker view, int selectedHour, int selectedMinute) {
			//int hour, minute;
			hour = selectedHour;
			minute = selectedMinute;
			timeButton.setText(new StringBuilder().append(pad(hour))
					.append(":").append(pad(minute)));
			//			StringBuilder timeUp= new StringBuilder().append(pad(hour))
			//					.append(":").append(pad(minute));
			//			String time_up_var = timeUp.toString();
		}
	};

	public static DatePickerDialog.OnDateSetListener datePickerListener 
	= new DatePickerDialog.OnDateSetListener() {

		// when dialog box is closed, below method will be called.
		public void onDateSet(DatePicker view, int selectedYear,
				int selectedMonth, int selectedDay) {
			year = selectedYear;
			month = selectedMonth + DB.CORRECT_DATE_TIME_VALUE;
			day = selectedDay;
			StringBuilder sTime = new StringBuilder().append(pad(day))
					.append("-").append(pad(month)).append("-").append(pad(year));
			dateButton.setText(sTime.toString());
		}
	};

	static String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

	private String stringBuilderDateTime() {
		String sTime = new StringBuilder().append("D[").append(dateButton.getText()).append("]T[").append(timeButton.getText()).append("]").toString();
		return sTime;
	}
	
	
}
