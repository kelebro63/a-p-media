package pandarium.android.calendar.database;

import java.util.List;

import pandarium.android.R;
import pandarium.android.calendar.model.Event;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

//Creating an Adapter Class
public class ListAdapter extends ArrayAdapter {
	private List<Event> items;
	private Context context;
	private static SparseBooleanArray mSelectedItemsIds;
	
	public ListAdapter(Context context, int textViewResourceId,
			List<Event> items) {
		
		super(context, textViewResourceId, items);
		this.items = items;
		this.context = context;
		mSelectedItemsIds = new SparseBooleanArray();
	}
	public View getCustomView(int position, View convertView,
			ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.list_view_item, parent, false);
		TextView tvName = (TextView) layout
				.findViewById(R.id.txtTitle);
		tvName.setText(items.get(position).getHeader());
		TextView tvDesc = (TextView) layout
				.findViewById(R.id.textDesc);
		tvDesc.setText(items.get(position).getDescription());
		TextView tvData = (TextView) layout
				.findViewById(R.id.txtData);
		tvData.setText(items.get(position).getCdi().toString());
		layout.setTag(items.get(position).getId());
		layout.setBackgroundColor(mSelectedItemsIds.get(position) ?  Color.parseColor("#97e397")
				: Color.parseColor("#D3F3D3"));
		layout.invalidate();
		layout.setId((items.get(position).getId())); //������ ������ 0
		return layout;
	}
	
	@Override
	public View getDropDownView(int position, View convertView,
			ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}
	
	public static void toggleSelection(int tag) {
		selectView(tag, !mSelectedItemsIds.get(tag));
		// notifyDataSetChanged();
	}
	
	public static void selectView(int position, boolean value) {
		if (value)
			mSelectedItemsIds.put(position, value);
		else
			mSelectedItemsIds.delete(position);
	}
	
	public static int getSelectedCount() {
		return mSelectedItemsIds.size();
	}

	
	public static SparseBooleanArray getSelectedIds() {
		return mSelectedItemsIds;
	}
	
	public static void removeSelection() {
		mSelectedItemsIds = new SparseBooleanArray();
		//  notifyDataSetChanged();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}
}
