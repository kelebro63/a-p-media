package pandarium.android.calendar.database;

public class ConverterStringToDate {

	String dateTime;

	public ConverterStringToDate(String dateTime) {
		super();
		this.dateTime = dateTime;
	}

	public int getDay(){
		return Integer.parseInt(dateTime.substring(2, 4));
	}

	public int getMonth(){
		return Integer.parseInt(dateTime.substring(5, 7));
	}

	public int getYear(){
		return Integer.parseInt(dateTime.substring(8, 12));
	}

	public int getHour(){
		return Integer.parseInt(dateTime.substring(15, 17));
	}

	public int getMinute(){
		return Integer.parseInt(dateTime.substring(18, 20));
	}

}
