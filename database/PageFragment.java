package pandarium.android.calendar.database;

import java.util.ArrayList;
import pandarium.android.EntryActivity;
import pandarium.android.R;
import pandarium.android.calendar.model.Event;
import pandarium.android.calendar.model.events.Alarm;
import pandarium.android.calendar.model.events.Events;
import pandarium.android.calendar.model.events.Note;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;



public class PageFragment extends Fragment implements OnItemLongClickListener, OnItemClickListener{

	static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
	int pageNumber;
	ListView lvMain_today;
	ArrayList<Event> events;
	static ActionMode mActionMode;
	static ListAdapter adapterForDel;
	static ListAdapter adapter = null;

	static PageFragment newInstance(int page) {

		PageFragment pageFragment = new PageFragment();
		Bundle arguments = new Bundle();
		arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
		pageFragment.setArguments(arguments);

		return pageFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.activity_page_fragment, null);
		lvMain_today = (ListView) view.findViewById(R.id.list);

		//��������� ������� � �������
		switch (pageNumber) {
		case 0:
			events = DataActivity.db_Alarms.getAllEvents(DB.DB_TABLE_ALARMS);
			break;
		case 1:
			events = DataActivity.db_Notes.getAllEvents(DB.DB_TABLE_NOTES);
			break;
		case 2:
			events = DataActivity.db_Events.getAllEvents(DB.DB_TABLE_EVENTS);
			break;
			//		case 3:
			//			events = DataActivity.db_Events.getAllEvents(DB.DB_TABLE_MEETINGS);
			//			break;
		default:
			break;
		}
		try {
			adapter = new ListAdapter(getActivity(), R.layout.list_view_item, events);
			lvMain_today.setAdapter(adapter);
		} catch (Exception e) {
			e.printStackTrace();
		}
		lvMain_today.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> arg0, View v,int pos, long id) {
				if (mActionMode != null) {
					onListItemSelect(pos);
					adapterForDel = (ListAdapter) arg0.getAdapter();
					adapterForDel.notifyDataSetChanged();
				} else {
					Intent intent = null;
					ListAdapter currentAdapter = (ListAdapter) arg0.getAdapter();
					Object event = null;//(Event) currentAdapter.getItem(pos);
					switch (pageNumber) {
					case 0:
						event = (Alarm) currentAdapter.getItem(pos);
						intent = new Intent(getActivity(), AlarmActivity.class);
						break;
					case 1:
						event = (Note) currentAdapter.getItem(pos);
						intent = new Intent(getActivity(), NoteActivity.class);
						break;
					case 2:
						event = (Events) currentAdapter.getItem(pos);
						intent = new Intent(getActivity(), EventActivity.class);
						break;

					default:
						break;
					}
					intent.putExtra("currentPagerList", pageNumber);
					intent.putExtra("test", (Parcelable) event);
					intent.putExtra("flag", 1);
					getActivity().startActivity(intent);
				}
			}
		});
		lvMain_today.setOnItemLongClickListener(new OnItemLongClickListener() {
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,int pos, long arg3) {
				onListItemSelect(pos); 				
				DataActivity.pager.getCurrentItem();
				adapterForDel = (ListAdapter) arg0.getAdapter();
				adapterForDel.notifyDataSetChanged();
				return true;
			}
		});


		return view;
	}


	@Override
	public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		return false;
	}  



	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub

	}	

	private void onListItemSelect(int position) {

		ListAdapter.toggleSelection(position);
		boolean hasCheckedItems = ListAdapter.getSelectedCount() > 0;

		if (hasCheckedItems && mActionMode == null)
			// there are some selected items, start the actionMode
		{

			mActionMode = ((ActionBarActivity) getActivity()).startSupportActionMode(new ActionModeCallback());//startActionMode(new ActionModeCallback());
			View ed = (View) getActivity().findViewById(R.layout.action_bar);
			mActionMode.setCustomView(ed);
			//getSherlockActivity().gets.setUiOptions(SActivityInfo.UIOPTION_SPLIT_ACTION_BAR_WHEN_NARROW)
			//	getSherlockActivity().getWindow().setUiOptions(ActivityInfo.UIOPTION_SPLIT_ACTION_BAR_WHEN_NARROW);
		}
		else if (!hasCheckedItems && mActionMode != null)
		{
			mActionMode.finish();
		}
		if (mActionMode != null){
			mActionMode.setTitle(String.valueOf(ListAdapter
					.getSelectedCount()) + " selected");
		}
		adapter.notifyDataSetChanged();
	}


	//	@Override
	//	public void onResume() {
	//		super.onResume();
	//		
	//	}


	class ActionModeCallback implements ActionMode.Callback {

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {

			// inflate contextual menu
			mode.getMenuInflater().inflate(R.menu.context_menu, menu);
			return true;
		}

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

			return false;
		}

		public boolean onActionItemClicked(ActionMode mode,  MenuItem item) {

			switch (item.getItemId()) {
			case R.id.menu_delete:

				//������� �� ����
				SparseBooleanArray selected = ListAdapter.getSelectedIds();
				for (int i = (selected.size() - 1); i >= 0; i--) {
					if (selected.valueAt(i)) {
						Event event = (Event)adapterForDel.getItem(selected.keyAt(i));
						switch (pageNumber) {
						case 0:
							DataActivity.db_Alarms.delEvent(event);
							break;
						case 1:
							DataActivity.db_Notes.delEvent(event);
							break;
						case 2:
							DataActivity.db_Events.delEvent(event);
							break;
						default:
							break;
						}
					}
				}
				mode.finish(); 
				break;
			default:
				return false;

			}
			return true;
		}

		@Override
		public  void onDestroyActionMode(ActionMode mode) {
			// remove selection
			ListAdapter.removeSelection();
			adapter.notifyDataSetChanged();
			try {
				DataActivity.update();
			} catch (Exception e) {
			}
			mActionMode = null;

		}

	}

	public static void closeActionMode() {
		mActionMode.finish();
	}
}

