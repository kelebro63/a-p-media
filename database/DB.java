package pandarium.android.calendar.database;

import java.util.ArrayList;
import java.util.List;

import pandarium.android.calendar.model.CDI;
import pandarium.android.calendar.model.Event;
import pandarium.android.calendar.model.events.Alarm;
import pandarium.android.calendar.model.events.Events;
import pandarium.android.calendar.model.events.Note;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

/**
 * Created with IntelliJ IDEA. <br>
 * <b>Autor :</b> Sergey Shustikov
 */
public class DB {

	//������������ ���
	public static final String DB_NAME_NOTES = "Notes";
	public static final String DB_NAME_ALARMS = "Alarms";
	public static final String DB_NAME_EVENTS = "Events";

	private static final int DB_VERSION = 1;
	//������������ ������
	public static final String DB_TABLE_NOTES = "Notes";
	public static final String DB_TABLE_ALARMS = "Alarms";
	public static final String DB_TABLE_EVENTS = "Events";
	//public static final String DB_TABLE_MEETINGS = "Meetings";

	//���� ������� Notes
	public static final String NOTES_COLUMN_ID = "_id";
	public static final String NOTES_COLUMN_TIME = "timestamp";
	public static final String NOTES_COLUMN_TITLE = "title";
	public static final String NOTES_COLUMN_DESC = "description";

	//���� ������� Alarms
	public static final String ALARMS_COLUMN_ID = "_id";
	public static final String ALARMS_COLUMN_TIME = "alarmtime";
	public static final String ALARMS_COLUMN_TITLE = "title";
	public static final String ALARMS_COLUMN_DESC = "description";
	public static final String ALARMS_COLUMN_REPEAT = "isRepeated";
	public static final String ALARMS_COLUMN_REPEAT_TYPE = "repeatType";

	//���� ������� BirthDays
	public static final String EVENTS_COLUMN_ID = "_id";
	public static final String EVENTS_COLUMN_DATE = "eventDate";
	public static final String EVENTS_COLUMN_TITLE = "title";
	public static final String EVENTS_COLUMN_DESC = "description";
	public static final String EVENTS_COLUMN_TYPE = "type";

	//���� ������� Meetings
	//	public static final String MEETINGS_COLUMN_ID = "_id";
	//	public static final String MEETINGS_COLUMN_DATE = "meetingsDate";
	//	public static final String MEETINGS_COLUMN_TITLE = "title";
	//	public static final String MEETINGS_COLUMN_DESC = "description";

	private final String PATH_ALARM_TABLE = "/data/data/pandarium.android/databases/Alarms";
	private final String PATH_NOTE_TABLE = "/data/data/pandarium.android/databases/Notes";
	private final String PATH_EVENT_TABLE = "/data/data/pandarium.android/databases/Events";

	public static final String[] NotesTableFields = {NOTES_COLUMN_ID, NOTES_COLUMN_TITLE, NOTES_COLUMN_DESC, NOTES_COLUMN_TIME};//
	public static final String[] AlarmsTableFields = { ALARMS_COLUMN_ID, ALARMS_COLUMN_TITLE, ALARMS_COLUMN_DESC, ALARMS_COLUMN_TIME, ALARMS_COLUMN_REPEAT_TYPE, ALARMS_COLUMN_REPEAT};// ALARMS_COLUMN_ID,
	public static final String[] EventsTableFields = {EVENTS_COLUMN_ID, EVENTS_COLUMN_TITLE, EVENTS_COLUMN_DESC, EVENTS_COLUMN_DATE, EVENTS_COLUMN_TYPE};// BIRTHDAYS_COLUMN_ID,
	//	public static final String[] MeetingsTableFields = {  MEETINGS_COLUMN_TITLE, MEETINGS_COLUMN_DESC, MEETINGS_COLUMN_DATE};//MEETINGS_COLUMN_ID,


	private static final String DB_NOTES_CREATE = 
			"create table " + DB_TABLE_NOTES + "(" +
					NOTES_COLUMN_ID + " integer primary key autoincrement, " +
					NOTES_COLUMN_TITLE + " text, " +
					NOTES_COLUMN_TIME + " text, " +
					NOTES_COLUMN_DESC + " text" +
					");";

	private static final String DB_ALARMS_CREATE = 
			"create table " + DB_TABLE_ALARMS + "(" +
					ALARMS_COLUMN_ID + " integer primary key autoincrement, " +
					ALARMS_COLUMN_TITLE + " text, " +
					ALARMS_COLUMN_DESC + " text, " +
					ALARMS_COLUMN_TIME + " text, " +
					ALARMS_COLUMN_REPEAT_TYPE + " text, " +
					ALARMS_COLUMN_REPEAT + " text" +
					");";

	private static final String DB_EVENTS_CREATE = 
			"create table " + DB_TABLE_EVENTS + "(" +
					EVENTS_COLUMN_ID + " integer primary key autoincrement, " +
					EVENTS_COLUMN_TITLE + " text, " +
					EVENTS_COLUMN_DESC + " text," +
					EVENTS_COLUMN_TYPE + " text," +
					EVENTS_COLUMN_DATE + " text" +
					");";

	//	private static final String DB_EVENTS_MEETINGS_CREATE = 
	//			"create table " + DB_TABLE_MEETINGS + "(" +
	//					MEETINGS_COLUMN_ID + " integer primary key autoincrement, " +
	//					MEETINGS_COLUMN_TITLE + " text, " +
	//					MEETINGS_COLUMN_DESC + " text," +
	//					MEETINGS_COLUMN_DATE + " text" +
	//					");";

	private static final int ALARM_TYPE_EVENT_INT = 1;
	private static final int NOTE_TYPE_EVENT_INT = 2;
	private static final int EVENTS_TYPE_EVENT_INT = 3;
	//private static final int MEETING_TYPE_EVENT_INT = 4;
	
	public static final  int CORRECT_DATE_TIME_VALUE = 1;
	private final Context mCtx;
	private int mCorrect;

	private DBHelper mDBHelper;
	private SQLiteDatabase mDB;

	public DB(Context ctx) {
		mCtx = ctx;
	}

	// ������� �����������
	public void open(String NameDB) {
		mDBHelper = new DBHelper(mCtx, NameDB, null, DB_VERSION);
		mDB = mDBHelper.getWritableDatabase();
	}

	// ������� �����������
	public void close() {
		if (mDBHelper!=null) mDBHelper.close();
	}

	// �������� ��� ������ �� ������� 
	public Cursor getAllData(String DB_TABLE) {
		return mDB.query(DB_TABLE, null, null, null, null, null, null);
	}

	public int addEvent(Event event) { 
		ContentValues cv = new ContentValues();
		ArrayList<String> dataFields =  getEventFields(event);
		String bdName = getdbName(getTypeEvent(event));
		//����� ����
		switch (getTypeEvent(event)) {
		case ALARM_TYPE_EVENT_INT:
			for (int i = 1; i < AlarmsTableFields.length; i++) {
				int j = i-1;
				cv.put(AlarmsTableFields[i], dataFields.get(j));
			}
			break;
		case NOTE_TYPE_EVENT_INT:
			for (int i = 1; i < NotesTableFields.length; i++) {
				int j = i-1;
				cv.put(NotesTableFields[i], dataFields.get(j));
			}
			break;	
		case EVENTS_TYPE_EVENT_INT:
			for (int i = 1; i < EventsTableFields.length; i++) {
				int j = i-1;
				cv.put(EventsTableFields[i], dataFields.get(j));
			}
			break;		
		default:
			break;
		}
		long id = mDB.insert(bdName, null, cv);

		Toast.makeText(mCtx, "�������� ������� � ����",
				Toast.LENGTH_LONG).show();
		return (int) id;
	}

	public Boolean delEvent(Event event) {

		//try {
		String bdName = getdbName(getTypeEvent(event));
		mDB.delete(bdName,  "_id = " + String.valueOf(event.getId()), null); 
		return true;
		//} catch (Exception e) {
		//	return false;
		//}
	}

	public Boolean delEventId(long selectedItem) {

		try {
			//	String bdName = getdbName(getTypeEvent(event));
			mDB.delete(DB_NAME_ALARMS,  "_id = " + String.valueOf(selectedItem), null);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public ArrayList<Event> getEventForId(long selectedItem, String table) {

		
			try {
				String selection = ALARMS_COLUMN_ID + " = ?";
				String[] selectionArgs =  new String[] {String.valueOf(selectedItem)};
				Cursor cursor =  getCursor(table, null, selection, selectionArgs, null, null, null);
				ArrayList<Event> events = convertCursor(cursor, table);
				return events;
			} catch (Exception e) {

			}
			return null;
			
		
	}

	public Boolean updateEvent(Event event) { 
		try {
			ContentValues cv = new ContentValues();
			ArrayList<String> dataFields =  getEventFields(event);
			String bdName = getdbName(getTypeEvent(event));
			switch (getTypeEvent(event)) {
			case ALARM_TYPE_EVENT_INT:
				for (int i = 1; i < AlarmsTableFields.length; i++) {
					int j = i-1;
					cv.put(AlarmsTableFields[i], dataFields.get(j));
				}
				break;
			case NOTE_TYPE_EVENT_INT:
				for (int i = 1; i < NotesTableFields.length; i++) {
					int j = i-1;
					cv.put(NotesTableFields[i], dataFields.get(j));
				}
				break;	
			case EVENTS_TYPE_EVENT_INT:
				for (int i = 1; i < EventsTableFields.length; i++) {
					int j = i-1;
					cv.put(EventsTableFields[i], dataFields.get(j));
				}
				break;		
			default:
				break;
			}
			mDB.update(bdName, cv, "_id = ?", new String[] { String.valueOf(event.getId()) });
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}	
	}

	public ArrayList<Event> getAllEvents(String table) { 

		ArrayList<Event> events = null;

		try {
			Cursor cursor =  getCursor(table, null, null, null, null, null, null);
			//	Cursor cursor = mDB.query( table, null, null, null, null, null, null);
			events = convertCursor(cursor, table);
			return events;
		} catch (Exception e) {

		}
		return events;


	}

	public ArrayList<Event> getAllEventsforDate(String table, String dateTime) { //�������� ��� ������� �� ����

		ArrayList<Event> events = null;
		Cursor cursor = null;
		String[] dateStrings = {dateTime};
		switch (getTypeEvent(table)) {
		case 1:
			cursor = getCursor(table, null, ALARMS_COLUMN_TIME, dateStrings, null, null, null);
			break;
		case 2:
			cursor = getCursor(table, null, NOTES_COLUMN_TIME, dateStrings, null, null, null);
			break;
		case 3:
			cursor = getCursor(table, null, EVENTS_COLUMN_DATE, dateStrings, null, null, null);
			break;
			//		case 4:
			//			cursor = getCursor(table, null, MEETINGS_COLUMN_DATE, dateStrings, null, null, null);
			//			break;
		default:
			break;
		}
		try {
			events = convertCursor(cursor, table);
			return events;
		} catch (Exception e) {
		}
		return events;

	}

	private Cursor getCursor(String table, String[] columns,
			String selection, String[] selectionArgs, String groupBy, String having, String orderBy) {
		if (table.equals(DB.DB_TABLE_ALARMS)) {
			columns = DB.AlarmsTableFields;
		} else if (table.equals(DB.DB_TABLE_NOTES)) {
			columns = DB.NotesTableFields;
		} else if (table.equals(DB.DB_TABLE_EVENTS)){
			columns = DB.EventsTableFields;
		}
		Cursor cursor = mDB.query( table, columns, selection, selectionArgs, groupBy, having, orderBy);
		return cursor;
	}

	private String getdbName(int typeEvent) {
		switch (typeEvent) {
		case ALARM_TYPE_EVENT_INT:
			return DB_NAME_ALARMS;

		case NOTE_TYPE_EVENT_INT:
			return DB_NAME_NOTES;

		case EVENTS_TYPE_EVENT_INT:
			return DB_NAME_EVENTS;

			//		case MEETING_TYPE_EVENT_INT:
			//			return DB_NAME_EVENTS;


		default:
			break;
		}
		return null;		

	}


	private ArrayList<String> getEventFields(Event event) {
		ArrayList<String> dataFields = new ArrayList<String>();
		int typeEvent = getTypeEvent(event);

		dataFields.add(event.getHeader());
		dataFields.add(event.getDescription());
		dataFields.add(event.getCdi().toString());
		switch (typeEvent) {
		case ALARM_TYPE_EVENT_INT:
			Alarm alarmEvent = (Alarm) event;
			dataFields.add(alarmEvent.getmRepeatType()); //repeatType
			if (alarmEvent.isRepeated()) {
				dataFields.add("1");
			} else {
				dataFields.add("0");
			}

			break;
		case NOTE_TYPE_EVENT_INT:

			break;
		case EVENTS_TYPE_EVENT_INT:
			Events eEvent = (Events) event;
			dataFields.add(eEvent.getTypeEvent().toString()); //birthdayDate
			//dataFields.add(event.getTime()); //birthdayDate
			break;
			//		case MEETING_TYPE_EVENT_INT:
			//			dataFields.add(event.getCdi().toString()); //meetingDate 
			//			//dataFields.add(event.getTime()); //meetingDate 
			//			break;

		default:
			break;
		}		

		return dataFields;

	}

	private int getTypeEvent(Event event) { //�������� ��� �������
		try {
			if (event.getCdi().getEventType().name().equals(CDI.EventType.ALARM.toString())){
				return ALARM_TYPE_EVENT_INT;
			} else if (event.getCdi().getEventType().name().equals(CDI.EventType.NOTE.toString())){
				return NOTE_TYPE_EVENT_INT;
			} else if (event.getCdi().getEventType().name().equals(CDI.EventType.EVENTS.toString())){
				return EVENTS_TYPE_EVENT_INT;
			} 
		} catch (Exception e) {}

		//		if (event.getClass().getName().equals("pandarium.android.calendar.model.events.Alarm")){
		//			return ALARM_TYPE_EVENT_INT;
		//		} else if (event.getClass().getName().equals("pandarium.android.calendar.model.events.Note")){
		//			return NOTE_TYPE_EVENT_INT;
		//		} else if (event.getClass().getName().equals("pandarium.android.calendar.model.events.Birtday")){
		//			return BIRHTDAY_TYPE_EVENT_INT;
		//		}else if (event.getClass().getName().equals("pandarium.android.calendar.model.events.Meeting")){
		//			return MEETING_TYPE_EVENT_INT;
		//		}

		/*
		 * �� ���� ������ ���� ���� ��� ��� � �������� ��� ���� type
		 */
		if (mDB.getPath().equals(PATH_NOTE_TABLE)) {
			return NOTE_TYPE_EVENT_INT;
		} else if (mDB.getPath().equals(PATH_ALARM_TABLE)) {
			return ALARM_TYPE_EVENT_INT;
		} else if (mDB.getPath().equals(PATH_EVENT_TABLE)) {
			return EVENTS_TYPE_EVENT_INT;
		}
		return 0;
	}

	private int getTypeEvent(String table) { //�������� ��� ������� �� �������
		if (table.equals(DB_TABLE_ALARMS)){ 
			return ALARM_TYPE_EVENT_INT;
		} else if (table.equals(DB_TABLE_NOTES)){
			return NOTE_TYPE_EVENT_INT;
		} else if (table.equals(DB_TABLE_EVENTS)){
			return EVENTS_TYPE_EVENT_INT;
			//		}else if (table.equals(DB_TABLE_MEETINGS)){
			//			return MEETING_TYPE_EVENT_INT;
		}
		return 0;


	}

	private ArrayList<Event> convertCursor(Cursor c, String table) { //�������� ArrayList �� �������
		String header = "";
		String description = "";
		List<Event> events = new ArrayList<Event>();
		String[] tableFields = null;
		//Event event = new Event(header, description){};
		
		if (c != null) {
			if (c.moveToFirst()) {
				int i = -1;
				do { //��� ���� �� �������
					Event event = null;
					if (table.equals(DB_TABLE_ALARMS)) {
						tableFields = AlarmsTableFields;
						event = new Alarm(header, description){};
					} else if (table.equals(DB_TABLE_NOTES)) {
						tableFields = NotesTableFields;
						event = new Note(header, description){};
					} else if (table.equals(DB_TABLE_EVENTS)){
						tableFields = EventsTableFields;
						event = new Events(header, description){};
					}
					
					int j = -1;//-2;
					i = i + 1; //����� ������
					
					for (String cn : c.getColumnNames()) { //��� ����� �� ��������
						j = j + 1; //����� �������
						try {
							if (tableFields[j].equals(EVENTS_COLUMN_TITLE)) {
								event.setHeader(c.getString(c.getColumnIndex(cn))); 
							} else if (tableFields[j].equals(EVENTS_COLUMN_DESC)) {
								event.setDescription(c.getString(c.getColumnIndex(cn)));
							} else if (tableFields[j].equals(EVENTS_COLUMN_ID)) {
								event.setId(Integer.parseInt(c.getString(c.getColumnIndex(cn))));	
							} else if (tableFields[j].equals(ALARMS_COLUMN_TIME)){
								ConverterStringToDate converterAlarm = new ConverterStringToDate(c.getString(c.getColumnIndex(cn)));
								event.setCdi(new CDI.Builder().setType(CDI.EventType.ALARM).setYear(converterAlarm.getYear()).setMonth(converterAlarm.getMonth()).setDay(converterAlarm.getDay()).setHours(converterAlarm.getHour()).setMinutes(converterAlarm.getMinute()).build());
								//	event.setEventTime(c.getString(c.getColumnIndex(cn)));	
							} else if (tableFields[j].equals(ALARMS_COLUMN_REPEAT_TYPE)){
									((Alarm) event).setmRepeatType(c.getString(c.getColumnIndex(cn)));
							} else if (tableFields[j].equals(EVENTS_COLUMN_TYPE)){
								((Events) event).setTypeEvent(Events.findByAbbr(c.getString(c.getColumnIndex(cn))));
							} else if (tableFields[j].equals(NOTES_COLUMN_TIME)){
								ConverterStringToDate converterNote = new ConverterStringToDate(c.getString(c.getColumnIndex(cn)));
								event.setCdi(new CDI.Builder().setType(CDI.EventType.NOTE).setYear(converterNote.getYear()).setMonth(converterNote.getMonth()).setDay(converterNote.getDay()).setHours(converterNote.getHour()).setMinutes(converterNote.getMinute()).build());
								Boolean repeat = null;
								//	event.settimestamp(c.getString(c.getColumnIndex(cn)));
							} else if (tableFields[j].equals(EVENTS_COLUMN_DATE)){
								ConverterStringToDate converterEvent = new ConverterStringToDate(c.getString(c.getColumnIndex(cn)));
								event.setCdi(new CDI.Builder().setType(CDI.EventType.EVENTS).setYear(converterEvent.getYear()).setMonth(converterEvent.getMonth()).setDay(converterEvent.getDay()).setHours(converterEvent.getHour()).setMinutes(converterEvent.getMinute()).build());
								//	event.setbirthdayDate(c.getString(c.getColumnIndex(cn)));		
							} else if (tableFields[j].equals(ALARMS_COLUMN_REPEAT)){
								Boolean repeat = null;
								if (c.getInt(c.getColumnIndex(cn)) == 1) {
									repeat = true;
								} else {
									repeat = false;
								}
								((Alarm) event).setRepeated(repeat);
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					events.add(event);
				} while (c.moveToNext());
			}
		} else {
		}
		return (ArrayList<Event>) events;
	}

	// ����� �� �������� � ���������� ��
	private  class DBHelper extends SQLiteOpenHelper {

		String name;

		public DBHelper(Context context, String name, CursorFactory factory,
				int version) {
			super(context, name, factory, version);
			this.name = name;
		}

		// ������� � ��������� ��
		@Override
		public void onCreate(SQLiteDatabase db) {

			if (this.name == DB_NAME_NOTES) {
				db.execSQL(DB_NOTES_CREATE);
			} else if (this.name == DB_NAME_EVENTS) {
				db.execSQL(DB_EVENTS_CREATE);
				//	db.execSQL(DB_EVENTS_MEETINGS_CREATE);				
			} else if (this.name == DB_NAME_ALARMS){
				db.execSQL(DB_ALARMS_CREATE);
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			if (newVersion > oldVersion) {

				// so we need three SQL statements:
				try {
					//                        db.execSQL("ALTER TABLE " + DB_TABLE_ALARMS + " ADD COLUMN "
					//                                        + "testAlarms" + " TEXT;");
					//                        db.execSQL("ALTER TABLE " + DB_TABLE_NOTES + " ADD COLUMN "
					//                                        + "testNotes" + " INTEGER;");
					//                        db.execSQL("ALTER TABLE " + DB_TABLE_BIRTHDAYS + " ADD COLUMN "
					//                                        + "testNotes" + " TEXT;");
				} catch (SQLException e) {}
				//				Toast.makeText(mCtx, "���������� ��������� ��, ���������...",
				//						Toast.LENGTH_LONG).show();
			}

		}
	}

}
