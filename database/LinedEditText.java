package pandarium.android.calendar.database;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.EditText;


public class LinedEditText extends EditText {
	 private Rect mRect;
	    private Paint mPaint;

	    // we need this constructor for LayoutInflater
	    public LinedEditText(Context context, AttributeSet attrs) {
	        super(context, attrs);

	        mRect = new Rect();
	        mPaint = new Paint();
	        mPaint.setStyle(Paint.Style.STROKE);
	        mPaint.setColor(Color.GRAY);
	    }

	    @Override
	    protected void onDraw(Canvas canvas) {
	        int count = getLineCount();
	        //*
	        int height = this.getMeasuredHeight();
	        int line_height = this.getLineHeight();
	        int page_size = height / line_height + 1;

	        if (count < page_size) {
	            count = page_size;}
	            int posY = 5;
	        //*    
	        
	        Rect r = mRect;
	        Paint paint = mPaint;

	        for (int i = 0; i < count; i++) {
	        	posY +=  line_height;
	        	//int baseline = getLineBounds(i, r);

	          //  canvas.drawLine(r.left, baseline + 1, r.right, baseline + 1, paint);
	        	 canvas.drawLine(0, posY, getRight(), posY, mPaint);
	             canvas.save();

	        }

	        super.onDraw(canvas);
	    }
}
