package pandarium.android.calendar.database;

import java.util.concurrent.TimeUnit;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.CursorLoader;

public  class CalendarCursorLoader extends CursorLoader{
	
	DB db;
	String dbTableName;
    
    public  CalendarCursorLoader(Context context, DB db, String dbTableName) {
      super(context);
      this.db = db;
      this.dbTableName = dbTableName;
    }
    
    @Override
    public Cursor loadInBackground() {
      Cursor cursor = db.getAllData(this.dbTableName);
      return cursor;
    }
    
  }


