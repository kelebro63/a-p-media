package pandarium.android.calendar.model;

/**
 * Created by User on 20.01.14.                    //TODO: change <User> to real name. Use @autor tag.
 */

import pandarium.android.calendar.view.*;

import java.util.ArrayList;
import java.util.List;

// Class describes day of month
public class Day {

    private final int CORRECTION_FACTOR = 1;      //TODO: explain in JavaDoc what that mDayValue means.

    private byte mDayValue;
    private byte mIndexOfWeek;
    private List<Event> mListEvents;

    public Day(int value, int indexOfWeek){
        this.mIndexOfWeek = (byte) indexOfWeek;
        this.mDayValue = (byte) value;
        mListEvents = new ArrayList<Event>();
    }

    public void addEvent(Event event){
        mListEvents.add(event);
    }

    public void setEvents(List<Event> eventsList){
        this.mListEvents = eventsList;
    }

    public void removeEvent(int pos){
        mListEvents.remove(pos);
    }

    public void removeAllEvents(){
        mListEvents.clear();
    }

    public Event getEvent(int pos){
        return mListEvents.get(pos);
    }

    public byte getDayValue(){
        return mDayValue;
    }

    public byte getIndexOfWeek(){
        return mIndexOfWeek;
    }

    public String getDayOfWeekName(){
        return CalendarConstants.getDayNameById(getIndexOfWeek() - CORRECTION_FACTOR);
    }

    @Override
    public String toString(){
        return new StringBuilder()
                .append("Day ").append(mDayValue)
                .append(" with index ").append(mIndexOfWeek)
                .toString();
    }

    @Override
    public boolean equals(Object anotherDay){
        if(anotherDay instanceof Day) {
            boolean dayCompare = (mDayValue == ((Day) anotherDay).getDayValue());
            boolean indexCompare = (mIndexOfWeek == ((Day) anotherDay).getIndexOfWeek());
            return dayCompare && indexCompare;
        } else
            return false;
    }
}
