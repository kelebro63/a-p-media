package pandarium.android.calendar.model;

import pandarium.android.calendar.database.ConverterStringToDate;
import android.os.Parcel;
import android.os.Parcelable;

/** Created by User on 20.01.14.                             //TODO:change <User> to real name. Use @autor tag */
public abstract class Event implements Parcelable{
	
	private int id;
    private String mHeader;
    private String mDescription;
   
    private CDI cdi;
    //private String mTime;

    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
    
    public CDI getCdi() {
		return cdi;
	}

	public void setCdi(CDI cdi) {
		this.cdi = cdi;
	}

	public Event(String eventHeader, String eventDescription){
        this.mHeader = eventHeader;
        this.mDescription = eventDescription;
    }

    public String getDescription(){
        return mDescription;
    }

    public void setDescription(String mDescription){
        this.mDescription = mDescription;
    }

    public String getHeader(){
        return mHeader;
    }

    public void setHeader(String mHeader){
        this.mHeader = mHeader;
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(mHeader);
        out.writeString(mDescription);
        out.writeString(cdi.toString());
    }
    
    public Event(Parcel in){
        id = in.readInt();
        mHeader = in.readString();
        mDescription = in.readString();
        ConverterStringToDate converter = new ConverterStringToDate(in.readString());
        cdi = new CDI.Builder().setYear(converter.getYear()).setMonth(converter.getMonth()).setDay(converter.getDay()).setHours(converter.getHour()).setMinutes(converter.getMinute()).build(); //setType
    }

    @Override
    public String toString(){
        return new StringBuilder()
                .append("[Header  = ").append(mHeader)
                .append(", description = ").append(mDescription)
                .append("]")
                .toString();
    }

    @Override
    public boolean equals(Object anotherEvent){
        if(anotherEvent instanceof Event) {
            boolean headerCompare = (mHeader == ((Event) anotherEvent).getHeader());
            boolean descriptionCompare = (mDescription == ((Event) anotherEvent).getDescription());
            return headerCompare && descriptionCompare;
        } else
            return false;
    }
}
