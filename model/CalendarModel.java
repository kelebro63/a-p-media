package pandarium.android.calendar.model;

/***
 * Created with IntelliJ IDEA. <br>
 * <b>Autor :</b> Sergey Shustikov
 */

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class CalendarModel extends GregorianCalendar {   //������ ���������

    private final int FIRST_MONTH_NUMBER = 1;
    private final int MONTH_COUNT_IN_YEAR = 12;
    private final int FIRST_DAY_OF_MONTH = 1;
    
    private List<Month> monthList;
    private int currentYear;
    private int currentMonth;
    private int currentDayOfMonth;

    private static CalendarModel instance = null;

    private CalendarModel(){
        monthList = new ArrayList<Month>();
    }
    

    /** Set current year*/
    public void setYear(int year){
        getInstance().set(Calendar.YEAR, year);
    }
    
    public int getCurrentYear(){
    	int currentYear = getInstance().get(Calendar.YEAR);
    	return currentYear;
    }
    
    public int getDaysInMonth(int year, int monthIndex){
    	getInstance().setYear(year);
    	getInstance().set(Calendar.MONTH, monthIndex);
    	int daysInMonth = getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);	
    	return daysInMonth;
    }
    
    public void increaceYear(){
    	getInstance().set(Calendar.YEAR, getCurrentYear()+1);
    }
    
    public void decreaseYear(){
    	getInstance().set(Calendar.YEAR, getCurrentYear()-1);
    }
    public void setCurrentMonth( int index){
        getInstance().set(Calendar.MONTH, index);
    }

    /** Set and fill current year*/
    public List<Month> fillYear(int year){
        getInstance().set(Calendar.YEAR, year);
        monthList.clear();
        for (int i = FIRST_MONTH_NUMBER; i <= MONTH_COUNT_IN_YEAR; i++) {
            monthList.add(new Month(i));
        }
        return monthList;
    }

    /** Fill current year*/
    public List<Month> fillYear(){
        monthList.clear();
        for (int i = FIRST_MONTH_NUMBER; i <= MONTH_COUNT_IN_YEAR; i++) {
            monthList.add(new Month(i));
        }
        return monthList;
    }

    /** Return current months list*/
    public List<Month> getListMonth(){
        return monthList;
    }

    /** Return number of days in year*/
    public int getDaysInYear(){
        return getInstance().getActualMaximum(Calendar.DAY_OF_YEAR);
    }

    /** Return number of days in month*/
    public int getNumberDaysInMonth(int monthIndex){
        return monthList.get(monthIndex).getDaysCount();
    }

    /**  Return list of days of current month*/
    public List<Day> getDaysOfMonth(int monthIndex){
        return monthList.get(monthIndex).getDays();
    }

    /**Return day of week for the first day of current month*/
    public int getFirstDayOfMonthIndex(int year, int monthIndex){
        saveCurrentDate();
        getInstance().setYear(year);
    	getInstance().set(Calendar.MONTH, monthIndex);
    	getInstance().set(Calendar.DAY_OF_MONTH, FIRST_DAY_OF_MONTH); 	
    	int firstDayOfMonthIndex = getInstance().get(Calendar.DAY_OF_WEEK);
        loadCurrentDate(); //��������� ������� ����
    	return firstDayOfMonthIndex;
    }
    
    public void saveCurrentDate(){
    	currentYear = getInstance().getCurrentYear();
        currentMonth = getInstance().get(Calendar.MONTH);
        currentDayOfMonth = get(Calendar.DAY_OF_MONTH);
    }
    
    public void loadCurrentDate(){
    	getInstance().setYear(currentYear);
    	getInstance().setCurrentMonth(currentMonth);
    	getInstance().set(Calendar.DAY_OF_MONTH, currentDayOfMonth);
    }

    public static synchronized CalendarModel getInstance(){
        if(instance == null) {
            instance = new CalendarModel();
        }
        return instance;
    }
    
    
}
