package pandarium.android.calendar.model;

/**
 * Created with IntelliJ IDEA. <br>
 * <b>Autor :</b> Sergey Shustikov
 */

/** Calendar Data Identificator */
public class CDI {

    public static final String TIME_DIVIDER = "]T[";
    public static final String DATE_DIVIDER = "D[";

    public enum EventType {
        ALARM, NOTE, HOLIDAY, EVENTS;
    };

    private EventType eventType;

    public EventType getEventType(){
        return eventType;
    }

    public int getEventDay(){
        return eventDay;
    }

    public int getEventMonth(){
        return eventMonth;
    }

    public int getEventYear(){
        return eventYear;
    }

    public int getEventHH(){
        return eventHH;
    }

    public int getEventMM(){
        return eventMM;
    }

    private int eventDay;
    private int eventMonth;
    private int eventYear;
    private int eventHH;
    private int eventMM;

    public static class Builder {
        private static CDI mInstance;

        public Builder(){
            mInstance = new CDI();
        }

        public Builder setType(EventType type){
            mInstance.eventType = type;
            return this;
        }

        public Builder setDay(int day){
            mInstance.eventDay = day;
            return this;
        }

        public Builder setMonth(int month){
            mInstance.eventMonth = month;
            return this;
        }

        public Builder setYear(int year){
            mInstance.eventYear = year;
            return this;
        }

        public Builder setHours(int hours){
            mInstance.eventHH = hours;
            return this;
        }

        public Builder setMinutes(int minutes){
            mInstance.eventMM = minutes;
            return this;
        }

        public CDI build(){
            return mInstance;
        }
    }

    @Override
    public String toString(){
        return new StringBuilder(DATE_DIVIDER)
                .append(pad(eventDay)).append(".")
                .append(pad(eventMonth)).append(".")
                .append(pad(eventYear))
                .append(TIME_DIVIDER)
                .append(pad(eventHH)).append(":")
                .append(pad(eventMM)).append("]")
                .toString();
    }

    private String pad(int c) {
		if (c >= 10)
			return String.valueOf(c);
		else
			return "0" + String.valueOf(c);
	}

    @Override
    public boolean equals(Object o){
        if(o != null && o instanceof CDI) {
            CDI thisCDI = this;
            CDI newCDI = (CDI) o;
            return ((thisCDI.eventDay == newCDI.eventDay) &&
                    (thisCDI.eventMonth == newCDI.eventMonth) &&
                    (thisCDI.eventYear == newCDI.eventYear) &&
                    (thisCDI.eventHH == newCDI.eventHH) &&
                    (thisCDI.eventMM == newCDI.eventMM));
        }
        return false;
    }
}
