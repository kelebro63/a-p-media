package pandarium.android.calendar.model;

/***
 * Created by User on 20.01.14.                //TODO: change User to real name. use @autor tag.
 */

import pandarium.android.calendar.view.*;

import java.util.*;

public class Month {

    private final int CORRECTION_FACTOR = 1;     //TODO:explain what that value means in JD.
    private final int FIRST_DAY_OF_MONTH = 1;
    private final int SECOND_DAY_OF_MONTH = 2;
    private final int COUNT_DAYS_IN_WEEK = 7;

    private int mMonthIndex;
    private String mName;
    private byte mIndexOfFirstDay;
    private List<Day> mDayList;

    public Month(int index){
        this.mMonthIndex = index;
        this.mName = CalendarConstants.getMonthNameById(index);
        mDayList = new ArrayList<Day>();
        CalendarModel.getInstance().set(Calendar.MONTH, index);
        fillDaysList();
    }

    private void fillDaysList(){
        CalendarModel.getInstance().set(Calendar.DAY_OF_MONTH, FIRST_DAY_OF_MONTH);
        int maxDaysInMonth = CalendarModel.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH);
        byte dayOfWeek = (byte) (CalendarModel.getInstance().get(Calendar.DAY_OF_WEEK));

        // Initial set
        mDayList.add(new Day((byte) FIRST_DAY_OF_MONTH, dayOfWeek));
        mIndexOfFirstDay = dayOfWeek;

        for (int i = SECOND_DAY_OF_MONTH; i <= maxDaysInMonth; i++) {
            dayOfWeek++;
            if(dayOfWeek > COUNT_DAYS_IN_WEEK) {
                dayOfWeek = 0;
            }
            mDayList.add(new Day(i, dayOfWeek));
        }
    }

    /** Return list of days for the month */
    public List<Day> getDays(){
        return mDayList;
    }

    /** Return mMonthIndex of day of week for first day of month */
    public byte getIndexOfFirstDay(){
        return mIndexOfFirstDay;
    }

    /** Return mMonthIndex of month */
    public int getMonthIndex(){
        return mMonthIndex;
    }

    public String getName(){
        return mName;
    }

    public int getDaysCount(){
        return this.mDayList.size();
    }

    @Override
    public String toString(){
        return new StringBuilder()
                .append("[ Month index = ").append(mMonthIndex)
                .append(", name = ").append(mName)
                .append(", index of first day = ").append(mIndexOfFirstDay)
                .toString();
    }

    @Override
    public boolean equals(Object anotherMonth){
        if(anotherMonth instanceof Month) {
            boolean indexCompare = (mMonthIndex == ((Month) anotherMonth).getMonthIndex());
            boolean nameCompare = (mName.equals(((Month) anotherMonth).getName()));
            boolean indexOfFirstDayCompare = (mIndexOfFirstDay == ((Month) anotherMonth).getIndexOfFirstDay());
            return indexCompare && nameCompare && indexOfFirstDayCompare;
        } else
            return false;
    }
}
