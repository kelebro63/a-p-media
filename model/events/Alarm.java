package pandarium.android.calendar.model.events;

import pandarium.android.calendar.model.Event;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created with IntelliJ IDEA. <br>
 * <b>Autor :</b> Sergey Shustikov
 */
public class Alarm extends Event implements Parcelable{
	
	private String mRepeatType;
	private boolean isRepeated;
	 
	public Alarm(String eventHeader, String eventDescription){
        super(eventHeader, eventDescription);
    }
	
	 public String getmRepeatType() {
			return mRepeatType;
		}

		public void setmRepeatType(String mRepeatType) {
			this.mRepeatType = mRepeatType;
		}

		public Boolean isRepeated(){
			return isRepeated;
	    }

	    public void setRepeated(boolean isRepeated){
	        this.isRepeated = isRepeated;
	    }
		
		@Override
		public int describeContents() {
			
			return 0;
		}

		public void writeToParcel(Parcel out, int flags) {
	        super.writeToParcel(out, flags);
	        out.writeString(mRepeatType);
	        boolean[] array = {isRepeated};
	        out.writeBooleanArray(array);
	    } 
		
		public static final Parcelable.Creator<Alarm> CREATOR = new Parcelable.Creator<Alarm>() {
	        public Alarm createFromParcel(Parcel in) {
	            return new Alarm(in);
	        }

	        public Alarm[] newArray(int size) {
	            return new Alarm[size];
	        }
	    };

	    private Alarm(Parcel in) {
	        super(in);
	        mRepeatType = in.readString();
	        boolean[] myBooleanArr = new boolean[1];
	        in.readBooleanArray(myBooleanArr);
	        isRepeated = myBooleanArr[0];
	    }	
	    
}

