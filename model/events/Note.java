package pandarium.android.calendar.model.events;

import android.os.Parcel;
import android.os.Parcelable;
import pandarium.android.calendar.model.*;

/**
 * Created with IntelliJ IDEA. <br>
 * <b>Autor :</b> Sergey Shustikov
 */
public class Note extends Event implements Parcelable{

    public Note(String eventHeader, String eventDescription){
        super(eventHeader, eventDescription);
    }

    @Override
	public int describeContents() {
		
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
       // out.writeString(mRepeatType);
    } 
	
	public static final Parcelable.Creator<Note> CREATOR = new Parcelable.Creator<Note>() {
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    private Note(Parcel in) {
        super(in);
      //  mRepeatType = in.readString();
    }	
}
