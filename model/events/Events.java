package pandarium.android.calendar.model.events;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;
import pandarium.android.calendar.model.*;
import pandarium.android.calendar.model.CDI.EventType;


public class Events extends Event implements Parcelable{
	
	//private String mEventType;
	private TypeEvent typeEvent;
	
	public enum TypeEvent {
        BIRTHDAY, MEETING
    };
    
	 public TypeEvent getTypeEvent() {
		return typeEvent;
	}

	public void setTypeEvent(TypeEvent typeEvent) {
		this.typeEvent = typeEvent;
	}
	
	public static ArrayList<String> getArrayTypes() {
		ArrayList<String> typeEvents = new ArrayList<String>();
		for (TypeEvent tEvent : TypeEvent.values()) {
			typeEvents.add(tEvent.toString());
			}
		return typeEvents;
	}
	
	public Events(String eventHeader, String eventDescription){
        super(eventHeader, eventDescription);
    }

	@Override
	public int describeContents() {
		
		return 0;
	}

	public void writeToParcel(Parcel out, int flags) {
        super.writeToParcel(out, flags);
      //  out.writeString(typeEvent.toString());
        out.writeValue(typeEvent);
    } 
	
	public static final Parcelable.Creator<Events> CREATOR = new Parcelable.Creator<Events>() {
        public Events createFromParcel(Parcel in) {
            return new Events(in);
        }

        public Events[] newArray(int size) {
            return new Events[size];
        }
    };

    private Events(Parcel in) {
        super(in);
        typeEvent = (TypeEvent) in.readValue(Events.class.getClassLoader());
    }	
    
    public static TypeEvent findByAbbr(String event){
        for(TypeEvent v : TypeEvent.values()){
            if( (v.toString()).equals(event.toString())){
                return v;
            }
        }
        return null;
    }
}

