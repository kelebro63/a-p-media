package com.example.ur.tlt;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.widget.ArrayAdapter;



class PageRefresh extends AsyncTask<String, Void, Void> {
	
	
    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      MenuItemCompat.setActionView(
		      PageActivity.activity.mProgress, 
		      R.layout.actionview_progress); 
    }

    @Override
    protected Void doInBackground(String... urls) {
		ListLinks ll = new ListLinks();
		try {
			ll.start(urls);
		} catch (IOException e) {

			e.printStackTrace();
		}
      return null;
    }

    @Override
    protected void onPostExecute(Void result) {
      super.onPostExecute(result);
      
      PageActivity.activity.title.setText(PageActivity.titleList.get(0).toString());
      PageActivity.image.setImageBitmap(PageActivity.bitmap.get(0));
      PageActivity.text.setText(PageActivity.textList);
      MenuItemCompat.setActionView(
    		  PageActivity.activity.mProgress, 
		      null);
     
    }
    
    public static class ListLinks {
    	
    	
	    public void start(String[] args) throws IOException {
	     
	        String url = args[0];
	        Document doc = Jsoup.connect(url).get();
	        Element masthead = doc.select("div.is_article_text").first();  //����� ��������
	        Elements title = doc.select("title"); // ���������
	        Elements media = doc.select("[src].ArticleMainImage");
	        for (Element src : media) {
	            if (src.tagName().equals("img")){
	            String ImgSrc = src.attr("src");
	            InputStream input = new java.net.URL(ImgSrc).openStream();
                // Decode Bitmap
	            Bitmap currentBitmap = BitmapFactory.decodeStream(input);
	            PageActivity.bitmap.add(currentBitmap);
	            }
	            else
	                print("11"+" * %s: <%s>", src.tagName(), src.attr("abs:src"));
	        }

	        for (Element text : title) {
	            PageActivity.titleList.add(trim(title.text(), 100));   
	        }
	        
	        PageActivity.textList = masthead.outerHtml();//masthead.text();
	        PageActivity.textList = PageActivity.textList.replaceAll("<br /><br />","\n");
	        PageActivity.textList = PageActivity.textList.replaceAll("<(.*?)\\>","");
	        PageActivity.textList = PageActivity.textList.replaceAll("\n","\n \n");
	        PageActivity.textList = PageActivity.textList.replaceAll("&nbsp;","");
	        PageActivity.textList = PageActivity.textList.replaceAll("(.*?)\\>", "");
	        PageActivity.textList = PageActivity.textList.replaceAll("&amp;","");
	        PageActivity.textList = PageActivity.textList.replaceAll("&quot;","");
	        PageActivity.textList = PageActivity.textList.replaceAll("&laquo;","");
	        PageActivity.textList = PageActivity.textList.replaceAll("&raquo;","");
	    }

	    private static void print(String msg, Object... args) {
	        System.out.println(String.format(msg, args));
	    }

	    private static String trim(String s, int width) {
	        if (s.length() > width)
	            return s.substring(0, width-1) + ".";
	        else
	            return s;
	    }
	}

	 
  }
