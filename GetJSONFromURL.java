package com.example.testmap;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class GetJSONFromURL extends AsyncTask<String, String, String> {


	InputStream inputStream = null;
	String result = ""; 

	protected void onPreExecute() {
		
		MenuItemCompat.setActionView(
				MainActivity.activity.mProgress, 
				R.layout.actionview_progress);

	}

	@Override
	protected String doInBackground(String... params) {

		//�������� �� ����
		if (MainActivity.testInet() != true) {
			return null;
		}
		
		String url_select = "http://kelebro.hol.es/";
		ArrayList<NameValuePair> param = new ArrayList<NameValuePair>();

		try {
			// Set up HTTP post

			// HttpClient is more then less deprecated. Need to change to URLConnection
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url_select);
			httpPost.setEntity(new UrlEncodedFormEntity(param));
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();

			// Read content & Log
			inputStream = httpEntity.getContent();
		} catch (UnsupportedEncodingException e1) {
			Log.e("UnsupportedEncodingException", e1.toString());
			e1.printStackTrace();
		} catch (ClientProtocolException e2) {
			Log.e("ClientProtocolException", e2.toString());
			e2.printStackTrace();
		} catch (IllegalStateException e3) {
			Log.e("IllegalStateException", e3.toString());
			e3.printStackTrace();
		} catch (IOException e4) {
			Log.e("IOException", e4.toString());
			e4.printStackTrace();
		}
		// Convert response to string using String Builder
		try {
			BufferedReader bReader = new BufferedReader(new InputStreamReader(inputStream, "iso-8859-1"), 8);
			StringBuilder sBuilder = new StringBuilder();

			String line = null;
			while ((line = bReader.readLine()) != null) {
				sBuilder.append(line + "\n");
			}

			inputStream.close();
			result = sBuilder.toString();
			Log.d(MainActivity.TAG, "background");
			return result;
		} catch (Exception e) {
			Log.e("StringBuilding & BufferedReader", "Error converting result " + e.toString());
		}
		return result;
	} // protected Void doInBackground(String... params)

	protected void onPostExecute(String result) {
		int i;
		if (result == null) {
			MainActivity.activity.error();
		} else {


			//������� �����
			MainActivity.map.clear();

			//parse JSON data
			try {
				//result = "[{\"imei\":\" 44fd02f38e4a5c0c\",\"dolgota\":\"49.409\",\"shirota\":\"53.5409\",\"date\":\"211:53:43\"},{\"imei\":\"a2422857b2cccf4c\",\"dolgota\":\"49.4385\",\"shirota\":\"53.5142\",\"date\":\"11:22:09\"}]";
				JSONArray jArray = new JSONArray(result);  
				for(i=0; i < jArray.length(); i++) {
					
					JSONObject object = jArray.getJSONObject(i);
					String imei = object.getString("imei");
					String dolgota = object.getString("dolgota");
					String shirota = object.getString("shirota");
					String date = object.getString("date");
					Log.d(MainActivity.TAG, "onPostExecute");

					//��������� �� �����
					MainActivity.map.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(shirota), Double.valueOf(dolgota))).title(imei).snippet(date));;

					//��������� ����
					int id = MainActivity.db_User.getID(imei);
					if (id == -1) {
						MainActivity.db_User.add(DB.DB_TABLE_USERS,imei, shirota, dolgota, date);
					} else {
						MainActivity.db_User.update(DB.DB_TABLE_USERS, id, shirota, dolgota, date);
					}

					//	������� ������ �� ���� � ��������� 
					//	MainActivity.db_User.getAll();

					//������� ������
					CameraPosition cameraPosition = new CameraPosition.Builder()
					.target(new LatLng(Double.valueOf(shirota), Double.valueOf(dolgota)))
					.zoom(7)
					.bearing(0)
					.tilt(20)
					.build();
					CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
					MainActivity.map.animateCamera(cameraUpdate);

					//String tab1_text = jObject.getString("tab1_text");
					//int active = jObject.getInt("active");

				} // End Loop

			} catch (JSONException e) {
				Log.e("JSONException", "Error: " + e.toString());
			} // catch (JSONException e)
		}
		MenuItemCompat.setActionView(
				MainActivity.activity.mProgress, 
				null);
	}
} 

